# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]
### Changed
### Removed
### Added



## [1.0] - 2018-09-26
### Added
- gitignore
- update the lab images
- CHANGELOG created

### Changed
- Always run the update
### Removed

## [0.0] - 2018-09-25
### Added  
Initialized the base project

