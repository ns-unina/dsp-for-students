# dsp_unina_vagrant

Vagrantfile for DSP Blackhat

## Usage  
There are two types of Vagrantfile (remote or local). 
Use:  
```
 vagrant up
```

To run the box. 
If the remote download through "vagrant up " command is too slow you can use the local version: 
1. download the vagrantbox (or put into local directory the vagrant box taken out-of-band:   

```
# Remote approach
cd remove;
vagrant up



# OR (local approach)
cd local;
sh init.sh

# Then execute Vagrant up
vagrant up
```

Then execute vagrant up to run the box.
